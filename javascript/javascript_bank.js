// JavaScript OPP exercises

// Class Bank

class Bank {
    constructor() {
        this.customers = {};
    }

    // add customer
    addCustomer = function (name) {
        this.customers["name"] = name;
    }
}



class Customer {

    constructor(name, balance = 0) {
        this.name = name;
        this.balance = balance;

    }

    // deposit money
    deposit = function (amount) {
        if (amount > 0) {
            this.balance += amount;
        }
    }


    // print account
    printAccountSummary = function () {
        console.log("Hello " + this.name);
        console.log("Your balance is currently $" + this.balance);
    }


    // withdraw money
    withdraw = function (amount) {

        // verify there are enough money
        var verifyBalance = this.balance - amount;
        if (amount > 0 && verifyBalance >= 0) {
            this.balance -= amount;
        } else {
            console.log("You do not have enough money to withdraw");
        }
        // after withdraw action, print the balance
        console.log("Your balance is " + this.balance);
    }
}





