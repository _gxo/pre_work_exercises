/* Javascript Exercises
1. Reverse
Write a method that will take a string as input, and return a new string with
the same letters in reverse order.*/

function reverse(string) {
  var reversedString = "";
  for (var index = string.length - 1; index >= 0; index--) {
    reversedString += string[index];
  }

  return reversedString;
}

reverse("Propulsion Academy");  // "ymedacA noisluporP" 
reverse("Hello"); // "olleH"
reverse("abcd"); // "dcba"


/*
2. Factorial
Write a method that takes an integer n in; it should return n*(n-1)*(n-2)*...*2*1.
Assume n >= 0.
As a special case, factorial(0) == 1. */

function factorial(n) {

  // check if it is the special case 
  if (n < 0) {
    return "n must be > or = to 0";

  } else {
    switch (n) {

      case 0:
        return 1;

      default:
        // assign the value of the integer
        var result = n;
        for (var i = n - 1; i > 0; i--) {
          result *= i;
        }
        return result;
    }
  }
}

factorial(5); // 120
factorial(4); // 24
factorial(0); // 1
factorial(-1); // "n must be > or = to 0"


/*
3. Longest Word
Write a method that takes in a string. Return the longest word in the string.
You may assume that the string contains only letters and spaces. You may use the
String split method to aid you in your quest.*/

function longest_word(sentence) {

  var words = sentence.split(' ');
  // an array to store the length of words
  var lengthsOfWords = [];

  // a loop to calculate the length of words
  words.forEach(function (word) {
    lengthsOfWords.push(word.length);
  })

  // find the index of the longest word
  var index = lengthsOfWords.indexOf(Math.max(...lengthsOfWords));

  return words[index];
}

longest_word("This is an amazing test") // "amazing"
longest_word("Laurent Colin") // "Laurent"
longest_word("Propulsion 123") // "Propulsion"


/*
4. Sum Nums 
Write a method that takes in an integer num and returns the sum of all integers
between zero and num, up to and including num.*/

function sum_nums(num) {

  // assign the value of the integer
  var sumNumbers = num;
  for (var nextNumber = num - 1; nextNumber > 0; nextNumber--) {
    sumNumbers += nextNumber;
  }

  return sumNumbers;
}

sum_nums(6) // 21
sum_nums(1) // 1
sum_nums(0) // 0


/*
5. Time Conversion
Write a method that will take in a number of minutes, and returns a string that
formats the number into hours:minutes.*/

function time_conversion(minutes) {

  // find the hours and minutes
  var hours = parseInt(minutes / 60);
  var min = parseInt(minutes % 60);

  // add an extra digit if needed (otherwise, for example, 61 minutes will return 1:1)
  var h = hours < 10 ? "0" + hours.toString() : hours;
  var m = min < 10 ? "0" + min.toString() : min;

  return h.toString() + ":" + m.toString();

}

time_conversion(155) // "02:35"
time_conversion(61) // "01:01"
time_conversion(60) // "01:00"
time_conversion(59) // "00:59"


/*
6. Count Vowels
Write a method that takes a string and returns the number of vowels in the string.
You may assume that all the letters are lower cased. You can treat “y” as a consonant.*/

function count_vowels(string) {
  var vowelsCount = 0;

  // array of vowels
  var v = ["a", "e", "i", "o", "u"];

  for (var i in string) {

    for (var letter of v) {
      if (string[i].toLowerCase() === letter) {
        vowelsCount += 1;
      }
    }
  }

  return vowelsCount;
}

count_vowels("alphabet") // 3
count_vowels("Propulsion Academy") // 7
count_vowels("AaaAa") // 5
count_vowels("fly") // 0


/*
7. Palindrome
Write a method that takes a string and returns true if it is a palindrome. A palindrome
is a string that is the same whether written backward or forward. Assume that there are
no spaces; only lowercase letters will be given.*/

function palindrome(string) {
  // reverse string
  var reversedString = "";
  for (var index = string.length - 1; index >= 0; index--) {
    reversedString += string[index];
  }

  // compare strings
  if (string.toLowerCase() === reversedString.toLowerCase()) {
    return true;
  } else {
    return false;
  }

}

palindrome("ABBA") // true
palindrome("AbbA") // true
palindrome("abcd") // false


/*
8. Nearby AZ
Write a method that takes a string in and returns true if the letter “z” appears
within three letters after an “a”. You may assume that the string contains only
lowercase letters.*/

function nearby_az(string) {

  // create arrays of indexes for both letters
  var alpha = [];
  var zeta = [];

  for (var index = 0; index <= string.length; index++) {
    if (string[index] === "a") {
      alpha.push(index);

    } else if (string[index] === "z") {
      zeta.push(index)
    }
  }

  // compare all possible pairs of indexes
  for (var index_a of alpha) {

    for (var index_z of zeta) {
      // use the absolute value
      if (Math.abs(index_a - index_z) <= 3) {
        return true;
      }
    }
  }

  return false;
}

nearby_az("abbbz") // false
nearby_az("abz") // true
nearby_az("abcz") // true
nearby_az("abba") // false


/*
9. Two Sum
Write a method that takes an array of numbers. If a pair of numbers in the array sums to
zero, return the positions of those two numbers. If no pair of numbers sums to zero,
return null.*/

function two_sum(nums) {

  // create array of possible pairs
  var pairs = [];

  // compare all possible pairs of numbers
  for (var index_a = 0; index_a <= nums.length; index_a++) {

    // use index_a + 1 to avoid duplicate records
    for (var index_b = index_a + 1; index_b <= nums.length; index_b++) {

      // compare values
      if ((nums[index_a] + nums[index_b]) === 0) {
        pairs.push([index_a, index_b]);
      }
    }
  }

  return pairs.length !== 0 ? pairs : null;

}

two_sum([1, 3, -1, 5]) // [[0, 2]]
two_sum([1, 3, -1, 5, -3]) // [[0, 2], [1, 4]]
two_sum([1, 5, 3, -4]) // null


/*
10. Is Power of Two

Write a method that takes in a number and returns true if it is a power of 2.
Otherwise, return false. You may want to use the % modulo operation. 5 % 2 returns
the remainder when dividing 5 by 2; therefore, 5 % 2 == 1. In the case of 6 % 2,
since 2 evenly divides 6 with no remainder, 6 % 2 == 0.*/

function is_power_of_two(num) {

  // create variable pow
  var pow = 1;

  // check if the given number is divided by 2
  if (num % 2 === 0) {
    do {
      // create a test value in order to compare it with the given number
      var value = Math.pow(2, pow)
      pow++;

      if (value === num) {
        return true;
      }

      // break the loop when the test value becomes larger than the given number
    } while (value < num);

  } // if the number is not divided by 2 return false
  else return false;

  // if the number is divided by 2 but it is not power of 2 return false
  return false;
}

is_power_of_two(8) // true
is_power_of_two(16) // true
is_power_of_two(32) // true
is_power_of_two(12) // false
is_power_of_two(24) // false


/*
11. Repeat a string
Repeat a given string (first argument) num times (second argument). Return an empty string
if num is not a positive number.*/

function repeat_string_num_times(str, num) {

  repeatedString = "";

  if (num < 0) {
    return "";
  }
  else {
    switch (num) {
      case 0:
        return "";

      case 1:
        return str;

      default:
        for (var times = 0; times < num; times++) {
          repeatedString += str;
        }
        return repeatedString;
    }
  }
}

repeat_string_num_times("abc", 3) // 'abcabcabc'
repeat_string_num_times("abc", 1) // 'abc'
repeat_string_num_times("abc", 0) // ''
repeat_string_num_times("abc", -1) // ''


/*
12. Sum All Numbers in a Range
Write a function that receives an array of two numbers as argument and returns the sum
of those two numbers and all numbers between them.*/

function add_all(arr) {

  // assign the base value
  var totalSum = arr[0];

  // loop as many times as the difference between the 2 values of the array
  for (var number = arr[0] + 1; number <= arr[1]; number++) {
    totalSum += number;
  }
  return totalSum;
}

add_all([1, 4]) // 10
add_all([5, 10]) // 45
add_all([9, 10]) // 19
add_all([0, 0]) // 0
add_all([-1, 1]) // 0


/*
13. True or False
Write a function that checks if a value is classified as a boolean primitive.
Return true or false.
Boolean primitives are true and false.*/

function is_it_true(args) {
  
  if (typeof args === 'boolean') {
    return true;
  }

  return false;
}


is_it_true(true) // true
is_it_true(false) // true
is_it_true('true') // false
is_it_true(1) // false
is_it_true('false') // false


/*
14. Return Largest Numbers in Arrays
Write a function that receives an array with four nested array. The function returns
an array consisting of the largest number from each provided sub-array.*/

function largest_of_four(arr) {

  // create an array to store the max values
  var maxArray = [];

  // loop in nested arrays
  for (var nestedArray of arr) {

    // create a variable to store the max value of each nested array/
    // assign the first value
    var maxValue = nestedArray[0];

    for (var index = 1; index <= nestedArray.length; index++) {
      if (maxValue < nestedArray[index]) {
        maxValue = nestedArray[index]
      }
    }

    // push the max value of the nested array
    maxArray.push(maxValue);

  }

  return maxArray;
}

largest_of_four([[13, 27, 18, 26], [4, 5, 1, 3], [32, 35, 37, 39], [1000, 1001, 857, 1]]) // [27,5,39,1001]


/*
15. Is it an anagram?
Write a JavaScript function to check if a word is an anagram or not.*/

function isAnagram(test, original) {

  // check if the words have the same number of letters
  if (test.length === original.length) {

    // split the word into arrays of lowercase letters
    var testArray = test.toLowerCase().split('');
    var originalArray = original.toLowerCase().split('');

    // function to sort an array
    function sorted_arr(array) {

      // create temporary value to swap values
      var tempValue;

      for (var i = 0; i < array.length; i++) {

        for (var j = i + 1; j < array.length; j++) {

          if (array[i] > array[j]) {
            tempValue = array[i];
            array[i] = array[j];
            array[j] = tempValue;
          }

        }
      }

      // return the sorted array
      return array;
    }

    // create variables of sorted arrays
    var testArraySorted = sorted_arr(testArray);
    var originalArraySorted = sorted_arr(originalArray);

    // compare the letters between the sorted arrays
    for (var iterator in testArraySorted) {
      if (testArraySorted[iterator] !== originalArraySorted[iterator]) {
        return false;
      }
    }
    // return true if all the pairs are the same
    return true;

  }

  // return false if the words don't have the same number of letters
  return false;
};

isAnagram("foefet", "toffee") // true
isAnagram("Buckethead", "DeathCubeK") // true
isAnagram("Twoo", "WooT") // true
isAnagram("dumble", "bumble") // false
isAnagram("ound", "round") // false
isAnagram("apple", "pale") // false