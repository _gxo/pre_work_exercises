# Python Basics Solutions

# 1. Command Line Arguments
def hello_world():
    name = input("Give me your name \n")
    if(name == '' ):
        print("Hello, Unkwown!")
    else:
        print("Hello, " + name + "!")




# 2. List of all files and directories names
import os

def files_and_dirlist():
    
    # Get working directory
    wdPath = os.getcwd()
    files = os.listdir(wdPath)    

    # Print all the files and directories
    for file in files:
       print(file)

print(files_and_dirlist())




# 3. Arrays are equal
arr1 = [2, 5, 7, 9, 11]
arr2 = [2, 5, 7, 8, 11]
arr3 = [2, 5, 11, 9, 7]

def are_two_arrays_equal(arg1, arg2):

    for index in range(0, len(arg1)):
        if sorted(arg1)[index] != sorted(arg2)[index]:
            return False

    return True
            



# 4. Middle character of a string
def get_middle_character(arg):
    
    # Get length of string
    stringLength = len(arg)

    # string with odd number of leters
    if(stringLength % 2 == 1):
        # return middle letter
        return arg[int((stringLength / 2) - 0.5)]
    else:
        # relse, return both letters
        return arg[int(stringLength/2) - 1] + arg[int(stringLength/2)]
                       
 
                      

# 5. Vowels in a string 
def count_of_vowels(string):
    
    vowelsCount = 0

    #array of vowels
    vowels = ["a", "e", "i", "o", "u"]
    
    # loop for ever string letter
    for i in range (0, len(string)):
        for letter in vowels:
            if (string[i].lower() == letter):
                vowelsCount += 1
    
    return vowelsCount;




# 6. Check valid password
def is_valid_password(password):
    
    special = ["!", "@", "$", "#", "%", "&", "*"]

    # 1st condition
    def checkLength(password):
        if(len(password) >= 10):
            return True
        else:
            return False
    
    # 2nd Condition
    def checkUpper(password):
        for letter in password:
            if(letter == letter.upper() and not letter.isdigit() and letter not
               in special):
                return True
            
        return False
    
    # 3rd Condition
    def checkSpecial(password):
        for letter in password:
            if(letter in special):
                return True
            
        return False
        
    # check all 3 conditions
    if(checkSpecial(password) and checkUpper(password) and checkLength(password)):
        return True
    else:
        return False




# 7. Find the second smallest element in an array
def find_second_smallest(arr):
    
    # sort array in ascending order
    for i in range(0 , len(arr) - 1):
        for j in range(i + 1 , len(arr)):
            if arr[i] > arr[j]:
                temp = arr[i]
                arr[i] = arr[j]
                arr[j] = temp
                
    return arr[1]
    



# 8. Remove duplicate elements from an array
def unique_array(arr):
    
    cleanArray = []
    
    for i in range(0 , len(arr)):
        if arr[i] not in cleanArray:
            cleanArray.append(arr[i])
                 
    return cleanArray




#9. Pentagonal numbers
def get_pentagonal_number(n): 
    pentagonalArray = []
    
    for i in range(1, n + 1):
        result = int((3 * i ** 2 - i) / 2)
        pentagonalArray.append(result)
    
    return pentagonalArray

get_pentagonal_number(50)




#10. Length of the longest sequence of zeros in binary representation of an integer
def get_size_of_longest_sequence_of_zeros(num):
    
    binaryRep = bin(num)

    
    counter = 0
    for i in range(2, len(binaryRep) - 1):
        if (binaryRep[i] == '0' and binaryRep[i + 1] == '0'):
            counter += 1
    
    return counter
    
    


# 11. Fibonacci
def fibonacci():
    
    num = input("Give an integer to calculate the fibonacci sequence \n")
    
    if num == '':
        return "Error: you must supply which Fibonacci number to compute"
    
    else:
        num = int(num)
        if num < 0:
            return "invalid input"
        
        elif num == 0:
            return 0
        
        elif num == 1:
            return 1
        
        else:
            fib = [0, 1]
            i = 2
            while (i <= num):
                fib.append(fib[i - 1] + fib[i - 2])
                i += 1 
            return fib[num]




# 12. Factorial
def factorial(n):
    
    # check if it is the special case n < 0 
    if (n < 0):
        return "n must be > or = to 0"
    
    # or if n = 0
    elif (n == 0):
        return 1
    
    else:
        result = n
        while n > 1:
            result *= n - 1
            n -= 1
        
    return result


# solution 2 - reduce method
import functools

def factorial_2(n):
    
    return functools.reduce(lambda x, y: x*y, list(range(1, n + 1)))    
     



#13. Prime Numbers
def prime_number(n):
    
    composites = []
    
    # find the composite numbers
    for i in range(1, n):
        for j in range(2, i - 1):
            if i % j == 0:
               composites.append(i)
               break
               
    
    primes = []
    
    # numbers that are not composite are primes
    for p in range(2, n):
        if p not in composites:
            primes.append(p)
    
    return primes




#14. Sorting an Array
from math import floor

def sort_it(arr):
    
    # round the array's values
    arr_rounded = map(lambda x: floor(x), arr)

    # sort the whole array in ascending order
    for i in range(0 , len(arr) - 1):
        for j in range(i + 1 , len(arr)):
            if arr[i] > arr[j]:
                temp = arr[i]
                arr[i] = arr[j]
                arr[j] = temp
      
    # create two separate arrays to store the odd and even values
    arr_odd = []
    arr_even = []
       
    for number in arr_rounded:
        if number % 2 != 0:
            arr_odd.append(number)
        else:
            arr_even.append(number)

    
    # concatenate them  
    return list(arr_odd) + list(arr_even)




# 15. Find the Digit 
def find(number):
    
    # store the digits in an array
    digitsArray = [int(x) for x in str(number)]
    
    # make the first multiplication
    multi = functools.reduce(lambda x, y: x*y, digitsArray)
    
    # special case 1
    if 0 in digitsArray:
        return 1 # because if there is a zero the multiplication equals to zero
    
    # special case 2
    elif len(digitsArray) == 1:
        return 0
    
    else:
        
    # set the counter
        counter = 0
        
        while multi > 9:
            multi = functools.reduce(lambda x, y: x*y, digitsArray)
            digitsArray = [int(x) for x in str(multi)]
            counter += 1
        return counter


















