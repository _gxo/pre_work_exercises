from track import Track

class Player(object):
    def __init__(self, playerName):
        self.playerName = playerName
        self.trackList = []
        self.trackNumber = 0
        self.totalTracks = 0

    def add(self, track: Track):
        self.trackList.append(track)
        self.totalTracks += 1


    def play(self):
        print("Playing: {0} ({1}) by {2}".format(self.trackList[self.trackNumber].__dict__["title"],
                                                 self.trackList[self.trackNumber].__dict__["album"],
                                                 self.trackList[self.trackNumber].__dict__["artist"]))

    def previous(self):
        if self.trackNumber == 0:
            self.trackNumber = self.totalTracks -1
        else:
            self.trackNumber -= 1

    def next(self):
        if self.trackNumber >= self.totalTracks - 1:
            self.trackNumber = 0
        else:
            self.trackNumber += 1
    
    def selectTrack(self, trackNumber):
        self.trackNumber = trackNumber - 1

    def printTracksInfo(self):
        for index in range(1, len(self.trackList) + 1):
            print("Track {0} {1} ({2}) by {3}".format(index,
                                                     self.trackList[index - 1].__dict__["title"],
                                                     self.trackList[index - 1].__dict__["album"],
                                                     self.trackList[index - 1].__dict__["artist"]))
